#include <M5StickC.h>
#include <config.h>

// the setup routine runs once when M5StickC starts up
int percentComplete = 0;

void setup() {
  // initialize the M5StickC object
  M5.begin();

  /** idea:   
   *  There is a 1px rect around the screen to indicate in colour what screen you are on - printer, current print, settings
   *  The main part will be a progress bar that fills the entire screen with a percent in the middle, that adjusts based on tilt
   *  Under the percent will be time remaining.
   *  Screen res is 80 x 160
   */

  
  
  // Lcd display
//  M5.Lcd.fillScreen(WHITE);
//  delay(500);
//  M5.Lcd.fillScreen(RED);
//  delay(500);
//  M5.Lcd.fillScreen(GREEN);
//  delay(500);
//  M5.Lcd.fillScreen(BLUE);
//  delay(500);
//  M5.Lcd.fillScreen(BLACK);
//  delay(500);

  M5.Lcd.fillScreen(RED);
//  Inner screen black
//M5.Lcd.fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
  

//  // text print
//  M5.Lcd.fillScreen(BLACK);

  
  // draw graphic
//  delay(1000);
//  M5.Lcd.drawRect(15, 55, 50, 50, BLUE);
//  delay(1000);
//  M5.Lcd.fillRect(15, 55, 50, 50, BLUE);
//  delay(1000);
//  M5.Lcd.drawCircle(40, 80, 30, RED);
//  delay(1000);
//  M5.Lcd.fillCircle(40, 80, 30, RED);
//  delay(1000);
}

// the loop routine runs over and over again forever
void loop(){
  M5.update();
  if (M5.BtnA.wasPressed()) {
    percentComplete = 30;
  }

  int barPos = percentPosition(percentComplete);
  int percentY = barPos-30;
  int timeLeftY = barPos+5;

  // Print Status Screen
  // Percent done
  M5.Lcd.fillScreen(RED);
  M5.Lcd.fillRect(1, 1, 78, barPos, BLACK);

  M5.Lcd.setCursor(0, 10);
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setTextSize(1);
  

  // Print Status
  char pos[2];
  sprintf(pos,"%d",barPos);
  M5.Lcd.printf(pos);
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setTextSize(1);
  M5.Lcd.setCursor(22, percentY);
  M5.Lcd.setTextSize(3);
  char str[3];
  sprintf(str,"%d",percentComplete);
  M5.Lcd.printf(str);
  
  // Print time left
  M5.Lcd.setTextSize(1);
  M5.Lcd.setTextColor(BLACK);
  M5.Lcd.setCursor(20, timeLeftY);
  M5.Lcd.printf("1hr 05m");
  delay(300);

  percentComplete++;
  
  //rand draw 
//  M5.Lcd.fillTriangle(random(M5.Lcd.width()-1), random(M5.Lcd.height()-1), random(M5.Lcd.width()-1), random(M5.Lcd.height()-1), random(M5.Lcd.width()-1), random(M5.Lcd.height()-1), random(0xfffe));

}



//map(value, fromLow, fromHigh, toLow, toHigh)
//Parameters
//
//value: the number to map.
//fromLow: the lower bound of the value’s current range.
//fromHigh: the upper bound of the value’s current range.
//toLow: the lower bound of the value’s target range.
//toHigh: the upper bound of the value’s target range.



int percentPosition(int Percent){
  int result;
  // Detect Orientation..
  int screenHeight = 158; // Minus 2 for 1px border
 int barOffset = map(Percent, 0, 100, 0, 158);
//  result = screenHeight - barOffset;
  result = screenHeight - Percent;
  return result;
}
